# Zoneminder
This repository contains Docker source code for the ZoneMinder project.

## Usage

The approach is very simple. This image is paired down to a minimal container image with NGINX, PHP-FPM, and ZoneMinder packages installed. It is required to run an external *empty* database with root credentials. Enter the needed user credentials and database name as environment variables into the ZoneMinder container. If necessary, it will create the database.

```yaml
version: '3.9'
services:
    zm:
        image: registry.gitlab.com/homelabio-public/zoneminder:latest
        shm_size: 512M
        depends_on:
            - db
        environment:
            ZM_DB_HOST: "db"
            ZM_DB_NAME: "zm"
            ZM_DB_USER: "foo"
            ZM_DB_PASS: "bar"
            ZM_DB_ROOT_PASSWORD: "bar"
            TZ: "America/New_York"
        ports:
            - 127.0.0.1:8081:8080

    db:
        image: mysql:5.7.37
        environment:
            MYSQL_ROOT_PASSWORD: "bar"
            TZ: "America/New_York"
        ports:
            - 127.0.0.1:3306:3306
        volumes:
            - db:/var/lib/mysql

volumes:
    db:
    data:
```

```bash
docker-compose-up -d
```
