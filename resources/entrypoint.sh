#!/bin/bash

set -eou pipefail

remoteDB=1
PHPINI=/etc/php7/php.ini
ZMCREATE=/usr/share/zoneminder/db/zm_create.sql
ZMCONF=/etc/zm/zm.conf
ZMPKG=/usr/bin/zmpkg.pl
ZMUPDATE=/usr/bin/zmupdate.pl
ZMCONF=/etc/zm/zm.conf

# Set the timezone before we start any services
sed -i -e "s|^;date.timezone.*$|date.timezone = ${TZ:-UTC}|g" "${PHPINI}"
if [ -L /etc/localtime ]; then
    ln -sf "/usr/share/zoneinfo/$TZ" /etc/localtime
fi
if [ -f /etc/timezone ]; then
    echo "$TZ" > /etc/timezone
fi

# Return status of mysql service
mysql_running () {
    if [ "$remoteDB" -eq "1" ]; then
        mysqladmin ping -u"${ZM_DB_USER}" -p"${ZM_DB_PASS}" -h"${ZM_DB_HOST}" > /dev/null 2>&1
    else
        mysqladmin ping > /dev/null 2>&1
    fi
    local result="$?"
    if [ "$result" -eq "0" ]; then
        echo "1" # mysql is running
    else
        echo "0" # mysql is not running
    fi
}

# Blocks until mysql starts completely or timeout expires
mysql_timer () {
    timeout=300
    count=0
    while [ "$(mysql_running)" -eq "0" ] && [ "$count" -lt "$timeout" ]; do
        echo "Mysql has not started up completely"
        sleep 2
        count=$((count+1))
    done

    if [ "$count" -ge "$timeout" ]; then
       echo " * Warning: Mysql startup timer expired!"
    fi
}

zm_db_exists() {
    if [ "$remoteDB" -eq "1" ]; then
        mysqlshow -u"${ZM_DB_USER}" -p"${ZM_DB_PASS}" -h"${ZM_DB_HOST}" "${ZM_DB_NAME}" > /dev/null 2>&1
    else
        mysqlshow zm > /dev/null 2>&1
    fi
    RETVAL=$?
    if [ "$RETVAL" = "0" ]; then
        echo "1" # ZoneMinder database exists
    else
        echo "0" # ZoneMinder database does not exist
    fi
}

# Check the status of the remote mysql server using supplied credentials
chk_remote_mysql () {
    if [ "$remoteDB" -eq "1" ]; then
        echo -n " * Looking for remote database server"
        if [ "$(mysql_running)" -eq "1" ]; then
            echo "   ...found."
        else
            echo "   ...failed!"
            return
        fi
        echo -n " * Looking for existing remote database"
        if [ "$(zm_db_exists)" -eq "1" ]; then
            echo "   ...found."
        else
            echo "   ...not found."
            RETVAL=$?
            if [ "$RETVAL" = "0" ]; then
                echo "   ...done."
            else
                echo "   ...failed!"
                echo " * Error: Remote database must be manually configred."
            fi
        fi
    else
        # This should never happen
        echo " * Error: chk_remote_mysql subroutine called but no sql credentials were given!"
    fi
}

## Needs work to handle upgrades
# ZoneMinder service management
# start_zoneminder () {
#     echo -n " * Starting ZoneMinder video surveillance recorder"
#     # Call zmupdate.pl here to upgrade the dB if needed. 
#     # Otherwise zm fails after an upgrade, due to dB mismatch
#     $ZMUPDATE --nointeractive
#     $ZMUPDATE --nointeractive -f

#     RETVAL=$?
#     if [ "$RETVAL" = "0" ]; then
#         echo "   dB upgrade passed or not needed ...done."
#     else
#         echo "   dB mismatch. upgrade ...failed!"
#         exit 1
#     fi
# }

# Configure NGINX
sed -i -e "s|@FPM_LISTEN_PORT@|${FPM_LISTEN_PORT}|g" \
       -e "s|@WEB_LISTEN_PORT@|${WEB_LISTEN_PORT}|g" \
       -e "s|@ZM_CACHEDIR@|${ZM_CACHEDIR}|g" \
       -e "s|@ZM_CGIDIR@|${ZM_CGIDIR}|g" \
       -e "s|@ZM_WEBDIR@|${ZM_WEBDIR}|g" \
       "${ZM_WEBCONF}"

# Configure for external Mysql
sed -i -e "s|^ZM_DB_HOST=.*$|ZM_DB_HOST=${ZM_DB_HOST}|g" \
       -e "s|^ZM_DB_NAME=.*$|ZM_DB_NAME=${ZM_DB_NAME}|g" \
       -e "s|^ZM_DB_USER=.*$|ZM_DB_USER=${ZM_DB_USER}|g" \
       -e "s|^ZM_DB_PASS=.*$|ZM_DB_PASS=${ZM_DB_PASS}|g" \
       "${ZMCONF}"

# Block until mysql starts completely or timeout expires
mysql_timer

# Create database schema if it doesn't exist
if [ "$(zm_db_exists)" -eq "0" ]; then
    echo " * First run of mysql in the container, creating ZoneMinder dB."
    mysql --user=root --password="${ZM_DB_ROOT_PASSWORD}" --host="${ZM_DB_HOST}" -e "CREATE USER '""${ZM_DB_USER}""'@'%' IDENTIFIED BY '""${ZM_DB_PASS}""';"
    mysql --user=root --password="${ZM_DB_ROOT_PASSWORD}" --host="${ZM_DB_HOST}" -e "GRANT ALL PRIVILEGES ON *.* TO '""${ZM_DB_USER}""'@'%';"
    mysql --user=root --password="${ZM_DB_ROOT_PASSWORD}" --host="${ZM_DB_HOST}" < "${ZMCREATE}"
else
    echo " * ZoneMinder dB already exists, skipping table creation."
fi

# tail logs while running
# tail -F /var/log/zoneminder/zm*.log /var/log/zm/zm*.log

exec supervisord -c /etc/supervisord.conf
