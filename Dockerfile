ARG TARGET_IMAGE_VERSION
FROM alpine:3.15

ARG VERSION=$TARGET_IMAGE_VERSION
ARG CONTAINER_USER=nobody

ENV FPM_LISTEN_PORT=9000
ENV WEB_LISTEN_PORT=8080
ENV ZM_WEBCONF="/etc/nginx/http.d/default.conf"
ENV ZM_CGIDIR="/usr/share/webapps/zoneminder/cgi-bin/"
ENV ZM_WEBDIR="/usr/share/webapps/zoneminder/htdocs/"
ENV ZM_CACHEDIR="/var/cache/zoneminder/"

LABEL name="ZoneMinder" \
      maintainer="Dana Dukes <drdukes@gmail.com>" \
      version=$VERSION \
      release=$VERSION \
      summary="The ZoneMinder CCTV Surveillance application as a microservice" \
      description="A full-featured, open source, state-of-the-art video surveillance software system."

RUN set -eux \
    && apk add --no-cache --upgrade \
           dumb-init \
           iputils \
           nginx \
           mysql-client \
           php7-fpm \
           php7-pdo \
           php7-pdo_mysql \
           supervisor \
           tzdata \
           zoneminder \
    && sed -i -e "s|^listen = .*$|listen = 127.0.0.1:${FPM_LISTEN_PORT}|g" \
              -e "s|^;access.log = .*$|access.log = /dev/stdout|g" \
              -e "s|^;slowlog = .*$|slowlog = /dev/stdout|g" \
              /etc/php7/php-fpm.d/www.conf \
    && sed -i -e "s|^;error_log = .*$|error_log = /dev/stdout|g" /etc/php7/php-fpm.conf \
    && mkdir -p /var/lib/zoneminder/events /var/log/zoneminder "${ZM_CACHEDIR}" /etc/supervisor.d \
    && chown "${CONTAINER_USER}" /etc \
    && chown -R "${CONTAINER_USER}:${CONTAINER_USER}" /etc/php7 /etc/zm /usr/share/webapps/zoneminder /var/lib/zoneminder /var/log/zoneminder /var/cache/zoneminder \
    && chown -R "${CONTAINER_USER}:${CONTAINER_USER}" /etc/nginx \
    && echo "error_log = /dev/stdout" >> /etc/php7/php.ini \
    && echo "php_admin_value[error_log] = /dev/stdout" >> /etc/php7/php-fpm.d/www.conf \
    && echo "security.limit_extensions = " >> /etc/php7/php-fpm.d/www.conf \
    && rm -fr /var/www/localhost/cgi-bin \
    && rm -fr /var/www/localhost/htdocs \
    && ln -s /usr/share/webapps/zoneminder/cgi-bin /var/www/localhost/cgi-bin \
    && ln -s /usr/share/webapps/zoneminder/htdocs /var/www/localhost/htdocs

COPY resources/entrypoint.sh /usr/bin/
COPY resources/nginx.conf "${ZM_WEBCONF}"
COPY resources/zoneminder.ini /etc/supervisor.d/

EXPOSE "${WEB_LISTEN_PORT}"

VOLUME /var/cache/zoneminder /var/log/zoneminder /var/lib/zoneminder/events

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["entrypoint.sh"]
